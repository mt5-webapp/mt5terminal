﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<WebTrader.Models.Views.ViewBalance>" %>

<%@ Import Namespace="WebTrader.Models.Core.Validator" %>
<%@ Import Namespace="WebTrader.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">Balance on MetaTrader 5</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <h1 class="interface">Balance on MetaTrader 5</h1>
    </div>
   <h1>Balance Detail</h1>
   <%
    if(Model.mtAccount !=null)
      {
    %>
        <table class="base_table" width="100%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <th style="width: 45px;">Login</th>
            <th style="width: 45px;">Credit</th>
            <th style="width: 45px;">Balance</th>
            <th style="width: 70px;">Profit</th>
            <th style="width: 60px;">CurrencyDigits</th>
            <th style="width: 60px;">Assets</th>
            <th style="width: 60px;">Liabilities</th>
            <th style="width: 60px;">Equity</th>
            <th style="width: 60px;">MarginLeverage</th>
        </tr>
    <tr>
        <td><%=Model.mtAccount.Login%></td>
        <td><%=Model.mtAccount.Credit%></td>
        <td><%=Model.mtAccount.Balance%></td>
        <td><%=Model.mtAccount.Profit%></td>
        <td><%=Model.mtAccount.CurrencyDigits%></td>
        <td><%=Model.mtAccount.Assets%></td>
        <td><%=Model.mtAccount.Liabilities%></td>
        <td><%=Model.mtAccount.Equity%></td>
        <td><%=Model.mtAccount.MarginLeverage%></td>
      
    </tr>
</table>
    <%
      }
     %>
</asp:Content>

