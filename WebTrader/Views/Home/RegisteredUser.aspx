﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<WebTrader.Models.Views.ViewRegister>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">Registration on MetaTrader 5</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <h1 class="interface">Registration on MetaTrader 5</h1>
    </div>
    <div class="formRegister" style="width:440px;margin: 0px auto;">
        <div>You registred on MetaTrader 5 Server</div>
        <div>Login: <%=Model.User.Login%></div>
        <div>Password: *******</div>
        <div>Investor Password <%=Model.User.InvestPassword%></div>
        <div>Main Password <%=Model.User.MainPassword%></div>
    </div>

     <script type="text/javascript">
        var accountId =  <%=Model.User.Login%>;
        function MetaTraderWebTerminal(f, c) {
            var d = "";
            try {
                 d = document.cookie.match(new RegExp("(?:^|; )" + "_wt_uniq".replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g,
                     "\\$1") + "=([^;]*)"))
             }
            catch (h) { }
            d = d ? decodeURIComponent(d[1]) : "";
            d || (d = window.localStorage ? window.localStorage.getItem("_wt_uniq") : void 0, d = d ? d : accountId);
            var b = d;
            try {
                 document.cookie = "_wt_uniq=" + b + "; path=/;"
             } catch (k) { }
            window.localStorage && window.localStorage.setItem("_wt_uniq", b);
            b = [];
            Boolean(c.mobile) && b.push("m=1");
            var a = c.version;
            4 != a && 5 != a || b.push("version=" + a);
            (a = c.login) && b.push("login=" + a);
            (a = c.server) && b.push("trade_server=" + a);
            (a = c.servers) && b.push("servers=" + a.join(","));
            a = c.startMode; "open_demo" !== a && "create_demo" !== a || b.push("startup_mode=" + a);
            Boolean(c.demoAllServers) && b.push("demo_all_servers=1");
            Boolean(c.demoAllowPhone) && b.push("demo_show_phone=1");
            a = c.language || c.lang; -1 !== "en ru de es pt zh ja ar bg fr id ms pl th tr vi ko hi uz uk da hu fa sk hr cs et sr sl nl fi el he it lv lt ro sv mn zt tg".indexOf(a) && b.push("lang=" + a);
            a = c.colorScheme; "black_on_white" !== a && "yellow_on_black" !== a && "green_on_black" !== a || b.push("color_scheme=" + a); (a = c.utmCampaign) && b.push("utm_campaign=" + a);
            (a = c.utmSource) && b.push("utm_source=" + a);
            !1 === c.savePassword && b.push("save_password=off");
            (a = c.symbols) && a.length && b.push("symbols=" + a.join(","));
            (a = c.demoType) && b.push("demo_type=" + a);
            (a = c.demoName) && b.push("demo_name=" + a);
            (a = c.demoFirstName) && b.push("demo_first_name=" + a);
            (a = c.demoSecondName) && b.push("demo_second_name=" + a);
            (a = c.demoEmail) && b.push("demo_email=" + a);
            (a = c.demoLeverage) && b.push("demo_leverage=" + a);
            var strWindowFeatures = "location=yes,height=900,width=1600,scrollbars=yes,status=yes";
            var URL = "https://trade.mql5.com/trade" + (b.length ? "?" + b.join("&") : "");
            var win = window.open(URL, "_blank", strWindowFeatures);
        }
        window.addEventListener('load', function () {
            console.log("All assets loaded accountId " + accountId);
            if (accountId > 0) {
                MetaTraderWebTerminal("webterminal", {
                    login: accountId,
                    version: 5,
                    server: "BLUEMARTINLTD-Server",
                    startMode: "login",
                    language: "en",
                    colorScheme: "yellow_on_black"
                });
            }
        });

    </script>
</asp:Content>

