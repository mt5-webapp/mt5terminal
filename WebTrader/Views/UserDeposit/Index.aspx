﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<WebTrader.Models.Views.ViewDeposit>" %>
<%@ Import Namespace="WebTrader.Models.Core.Validator" %>
<%@ Import Namespace="WebTrader.Helpers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">Registration on MetaTrader 5</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="text-align: center">
        <h1 class="interface">Deposit / Withdraw on MetaTrader 5</h1>
    </div>
<%
    using(Html.BeginFormValidated("UpdateBalance","UserDeposit",FormMethod.Post, new {style="margin: 0px;"}))
      {
%>
    <div class="formRegister" style="width:440px;margin: 0px auto;">
        <div style="margin: 5px auto;text-align: center;"><%=Html.Validator("common", new ValidatorBase())%></div>
        <div class="line" style="margin-top: 10px;">
            <label for="NewDepositAmount">Amount ( to withdraw enter negative amount) :</label>
            <div class="offset"><div class="inputWrapper"><%=Html.TextBox("NewDepositAmount", Model.mtDeposit.NewDepositAmount,
                                     new {style = "width: 240px", tabindex = "1", title = "Enter Amount please"})%></div></div>
        </div>
        <div class="offset">
            <%=Html.Validator("NewDepositAmount",new ValidatorEmpty("Amount is empty"))%>
        </div>
        <div class="offset" style="line-height:20px; vertical-align:top;">
            <input type="submit" tabindex="2" class="buttonActive" style="width: 100px;margin:10px 0px;" value="Update Balance" title="Update Balance"/>
        </div>
     </div>
<%
      }%>

</asp:Content>

