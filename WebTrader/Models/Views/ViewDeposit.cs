﻿//+------------------------------------------------------------------+
//|                                             MetaTrader 5 Web API |
//|                             Copyright 2000-2021, MetaQuotes Ltd. |
//|                                               www.metaquotes.net |
//+------------------------------------------------------------------+
using System.Collections.Generic;
using MetaQuotes.MT5WebAPI.Common;
//---
namespace WebTrader.Models.Views
  {
  public class ViewDeposit
    {
    public MtDeposit mtDeposit { get; set; }

    }

    public class MtDeposit { 
        public double NewDepositAmount { get; set; }
        public string Comment { get; set; }
        public MTDeal.EnDealAction Type { get; set; }

        public ulong Login { get; set; }
    }
  }