﻿//+------------------------------------------------------------------+
//|                                             MetaTrader 5 Web API |
//|                             Copyright 2000-2021, MetaQuotes Ltd. |
//|                                               www.metaquotes.net |
//+------------------------------------------------------------------+
using System.Collections.Generic;
using MetaQuotes.MT5WebAPI.Common;
//---
namespace WebTrader.Models.Views
  {
  public class ViewBalance
    {
    public MTAccount mtAccount { get; set; }

    }
  }