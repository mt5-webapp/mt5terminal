﻿//+------------------------------------------------------------------+
//|                                             MetaTrader 5 Web API |
//|                             Copyright 2000-2021, MetaQuotes Ltd. |
//|                                               www.metaquotes.net |
//+------------------------------------------------------------------+
using System.Web.Mvc;
using MetaQuotes.MT5WebAPI;
using MetaQuotes.MT5WebAPI.Common;
using WebTrader.Helpers;
using WebTrader.Models;
using WebTrader.Models.Core.Controllers;
using WebTrader.Models.Core.Web;
using WebTrader.Models.Users;
using WebTrader.Models.Views;
//---
namespace WebTrader.Controllers
{

    public class UserDepositController : BaseAsyncController
    {
        /// <summary>
        /// user login
        /// </summary>
        private ulong m_Login;
        /// <summary>
        /// current user data
        /// </summary>
        private MtDeposit mDep;

        public MTAccount mtAcc;
        /// <summary>
        /// does check user on MT server
        /// </summary>
        private bool m_IsCheckUser;
        /// <summary>
        /// ready to view tempalte
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexCompleted()
        {
            if (!UserState.IsLoggedIn)
            {
                return Http404NotFound("");
            } else
                m_Login = UserState.User.Login;

            MtDeposit mtdep = new MtDeposit() { NewDepositAmount = 0, Comment = "", Login = m_Login };
            ViewDeposit viewDep = new ViewDeposit { mtDeposit = mtdep};
            return View("Index", viewDep);
         
        }
        public void IndexAsync()
        {
            m_IsCheckUser = false;
            if (!UserState.IsLoggedIn)
            {
                Http404NotFound("");
                return;
            }
            else
                m_Login = UserState.User.Login;
        }

        public void UpdateBalanceAsync(MtDeposit mtDeposit)
        {
            if (mtDeposit.NewDepositAmount == 0) ViewData.ModelState.AddModelError("Amount", "Empty Name");
            if (ViewData.ModelState.IsValid)
            {
                AsyncManager.OutstandingOperations.Increment();
                QueueData dataInfo = new QueueData { Callback = OnNewDeposit };

                //--- Pass Amount from Page , as of now passed statically for testing purpose
                UserDeposit userDep = new UserDeposit
                {
                    Login = UserState.User.Login,
                    Type = MTDeal.EnDealAction.DEAL_BALANCE,
                    Comment = (mtDeposit.NewDepositAmount > 0 ? "Depositing " : "Withdrawing ") + " Balance ",
                    NewDepositAmount = mtDeposit.NewDepositAmount
                };
                dataInfo.Data = userDep;
                dataInfo.Name = "UserDepositChange";
                //---
                QueueRequest.Add(dataInfo);
            }
        }
        public void OnNewDeposit(MTRetCode result, object data)
        {

            if (result != MTRetCode.MT_RET_OK)
            {
                Log.Write(LogType.Error, "UserGet", "user not found, login: " + m_Login);
                ViewData.ModelState.AddModelError("common", "Balance could not be updated");
            }
            else
            {
                Log.Write(LogType.Info, "UserGet ", "user balance updated");
            }
            //---
            AsyncManager.OutstandingOperations.Decrement();
        }
        public ActionResult UpdateBalanceCompleted()
        {
            MtDeposit mtdep = new MtDeposit() { NewDepositAmount = 0, Comment = "", Login = m_Login };
            ViewDeposit viewDep = new ViewDeposit { mtDeposit = mtdep };
            return View("Index", viewDep);
        }

    }
}
