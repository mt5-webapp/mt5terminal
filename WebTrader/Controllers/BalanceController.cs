﻿//+------------------------------------------------------------------+
//|                                             MetaTrader 5 Web API |
//|                             Copyright 2000-2021, MetaQuotes Ltd. |
//|                                               www.metaquotes.net |
//+------------------------------------------------------------------+
using System.Web.Mvc;
using MetaQuotes.MT5WebAPI;
using MetaQuotes.MT5WebAPI.Common;
using WebTrader.Helpers;
using WebTrader.Models;
using WebTrader.Models.Core.Controllers;
using WebTrader.Models.Core.Web;
using WebTrader.Models.Users;
using WebTrader.Models.Views;
//---
namespace WebTrader.Controllers
{

    public class BalanceController : BaseAsyncController
    {
        /// <summary>
        /// user login
        /// </summary>
        private ulong m_Login;
        /// <summary>
        /// current user data
        /// </summary>
        private MTUser m_User;
        public MTAccount mtAcc;
        /// <summary>
        /// does check user on MT server
        /// </summary>
        private bool m_IsCheckUser;
        /// <summary>
        /// ready to view tempalte
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexCompleted()
        {
            if (!UserState.IsLoggedIn)
            {
                return Http404NotFound("");
            }
            //---
            ViewBalance viewBalance = new ViewBalance { mtAccount = mtAcc };
            return View("Index", viewBalance);
        }
        public void IndexAsync()
        {
            m_IsCheckUser = false;
            if (!UserState.IsLoggedIn)
            {
                Http404NotFound("");
                return;
            }
            //---
            if (!IsPostback)
            {
                //---
                AsyncManager.OutstandingOperations.Increment();
                QueueData dataInfo = new QueueData { Callback = OnUserAccountGet };
                //---
                CheckLogin checkLoginInfo = new CheckLogin { Login = UserState.User.Login };
                dataInfo.Data = checkLoginInfo;
                dataInfo.Name = "UserGet";
                //---
                QueueRequest.Add(dataInfo);
            }
        }
        public void OnUserAccountGet(MTRetCode result, object data)
        {

            if (result != MTRetCode.MT_RET_OK)
            {
                Log.Write(LogType.Error, "UserGet", "user not found, login: " + m_Login);
                ViewData.ModelState.AddModelError("common", "Login or password incorrect");
            }
            else
            {
                m_IsCheckUser = true;
                mtAcc = data as MTAccount;
                //--- format string
                string str = string.Format("Login: {0}, Credit : {1},Balance:{2}", mtAcc.Login, mtAcc.Credit, mtAcc.Balance);
                Log.Write(LogType.Info, "UserGet ", "user account detail :" + str);
            }
            //---
            AsyncManager.OutstandingOperations.Decrement();
        }


        ///// <summary>
        ///// Registration
        ///// </summary>
        //public void RegisterAsync(MTUser user)
        //{
        //    if (IsPostback)
        //    {
        //        m_User = user;
        //         //---
        //        if (!ViewData.ModelState.IsValid)
        //        {
        //            Log.Write(LogType.Error, "RegisterAsync", "some fields invalid");
        //            return;
        //        }
        //        //--- send data to MT server
        //        AsyncManager.OutstandingOperations.Increment();
        //        //---
        //        if (m_IsCheckUser)
        //        {
        //            AsyncManager.OutstandingOperations.Increment();
        //            QueueData dataInfo = new QueueData { Callback = OnNewDeposit };
        //            //---
        //            UserDeposit userDep = new UserDeposit { Login = m_Login, Type = MTDeal.EnDealAction.DEAL_BALANCE, Comment = "Depositing Balance 270", NewDepositAmount = 270 };
        //            dataInfo.Data = userDep;
        //            dataInfo.Name = "UserDepositChange";
        //            //---
        //            QueueRequest.Add(dataInfo);
        //        }
        //    }
        //    else
        //    {
        //        //---
        //        m_User = new MTUser();
        //    }
        //}

        //public void OnNewDeposit(MTRetCode result, object data)
        //{

        //    if (result != MTRetCode.MT_RET_OK)
        //    {
        //        Log.Write(LogType.Error, "UserGet", "user not found, login: " + m_Login);
        //        ViewData.ModelState.AddModelError("common", "Login or password incorrect");
        //    }
        //    else
        //    {
        //        //--- format string
        //        Log.Write(LogType.Info, "UserGet ", "user balance updated");
        //    }
        //    //---
        //    AsyncManager.OutstandingOperations.Decrement();
        //}

        //public ActionResult RegisterCompleted()
        //{
        //    ViewRegister view = new ViewRegister();
        //    //---
        //    view.User = m_User;
        //    //---
        //    if (!m_IsCheckUser)
        //    {
        //        //--- create captcha
        //        string code = Captcha.GenerateCaptchaText();
        //        //---
        //        UserHelper.VerificationCodeInSession = code;
        //        view.VerificationCode = code;
        //        //---
        //        return View("Register", view);
        //    }
        //    //--- registered users
        //    return View("RegisteredUser", view);
        //}
    }
}
